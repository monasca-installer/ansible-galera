# MariaDB Galera Cluster

Install [MariaDB Galera Cluster](https://mariadb.com/kb/en/mariadb/galera-cluster/)
when running installer in cluster mode.

## Role main tasks

- Install MariaDB and Galera packages.
- Remove unsafe defaults:
  - change root password
  - remove anonymous users
  - remove test database
- Configure network interface(s) to listen on.
- Configure and start synchronization.
- Create `clustercheck` script (for usage with proxy - e.g. HAProxy).
- Set up iptables.

## Role Variables

### Mandatory variables

|          Name          |                                                                             Description                                                                              |
|------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `galera_cluster_name`  | Sets Galera variable: `wsrep_cluster_name`.                                                                                                                          |
| `mariadb_galera_hosts` | Comma separated string with list of nodes on which MariaDB Galera will be installed. Format: "host1:galera_port,host2:galera_port". Used in: `wsrep_cluster_address` |
| `load_balancer_nodes`  | List of nodes that will be used as load balancer. Used for restricting connection to MariaDB ports to this hosts.                                                    |

### Other variables

|                   Name                   |                                                                      Default Value                                                                       |                                                   Description                                                    |
|------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------|
| `mariadb_version`                        | 10.1.18                                                                                                                                                  | Version of installed MariaDB libraries.                                                                          |
| `galera_version`                         | 25.3.18-1                                                                                                                                                | Version of installed Galera synchronization libraries.                                                           |
| `jemalloc_version`                       | 3.6.0-1                                                                                                                                                  | Version of installed jemalloc library.                                                                           |
| `mysql_port`                             | 3306                                                                                                                                                     | MariaDB access port.                                                                                             |
| `galera_port`                            | 4567                                                                                                                                                     | Galera port for synchronization.                                                                                 |
| `galera_incremental_state_transfer_port` | 4568                                                                                                                                                     | Galera Incremental State Transfer port.                                                                          |
| `galera_state_snapshot_transfer_port`    | 4444                                                                                                                                                     | Galera State Snapshot Transfer port.                                                                             |
| `clustercheck_port`                      | 9199                                                                                                                                                     | Port for `clustercheck` script.                                                                                  |
| `open_ports_for_galera`                  | - {"protocol": "tcp", "port": 4567}<br>- {"protocol": "udp", "port": 4567}<br>- {"protocol": "tcp", "port": 4568}<br>- {"protocol": "tcp", "port": 4444} | List of dicts with ports and protocols needed to open in iptables for Galera.                                    |
| `open_ports_for_load_balancer`           | - 3306<br>- 9199                                                                                                                                         | List of ports needed to be opened for load balancer.                                                             |
| `log_dir`                                | "/var/log"                                                                                                                                               | Directory for storing logs.                                                                                      |
| `mariadb_log_dir`                        | "/var/log/mariadb"                                                                                                                                       | Directory for storing MariaDB logs.                                                                              |
| `galera_error_log`                       | "/var/log/mariadb/mariadb-galera.err.log"                                                                                                                | All error logs from MariaDB and Galera will be send to this file.                                                |
| `clustercheck_systemd_log`               | "/var/log/mariadb/clustercheck.systemd.log"                                                                                                              | File for systemd logs from `clustercheck` script.                                                                |
| `clustercheck_error_log`                 | "/var/log/mariadb/clustercheck.err.log"                                                                                                                  | File for error logs from `clustercheck` script.                                                                  |
| `mariadb_user`                           | 'mysql'                                                                                                                                                  | The system user for MariaDB.                                                                                     |
| `mariadb_group`                          | "mysql"                                                                                                                                                  | The system group for MariaDB.                                                                                    |
| `mariadb_galera_deb_src_url`             | "http://yum.mariadb.org/10.1/centos7-amd64/rpms"                                                                                                         | MariaDB Galera rpm source URL.                                                                                   |
| `administrator_user`                     | "root"                                                                                                                                                   | MariaDB administrator user.                                                                                      |
| `administrator_password`                 | ""                                                                                                                                                       | MariaDB administrator password.                                                                                  |
| `socket_path`                            | "/var/lib/mysql/mysql.sock"                                                                                                                              | MariaDB socket path.                                                                                             |
| `galera_cluster_bind_address`            | "0.0.0.0"                                                                                                                                                | Sets Galera variable: `bind-address`.                                                                            |
| `galera_starting_node`                   | "127.0.0.1"                                                                                                                                              | Starting node for the Galera cluster.                                                                            |
| `clustercheck_db_user`                   | "clustercheck"                                                                                                                                           | Database user for clustercheck script.                                                                           |
| `clustercheck_db_password`               | "clustercheck"                                                                                                                                           | Database password for user used by clustercheck script.                                                          |
| `clustercheck_service_msg`               | "MariaDB Galera Cluster database health check"                                                                                                           | Clustercheck service and socket description.                                                                     |
| `run_mode`                               | "Deploy"                                                                                                                                                 | One of Deploy, Stop, Install, Start, or Use. The default is Deploy which will do Install, Configure, then Start. |

## `clustercheck` script

This role create `clustercheck` script on every node to make a proxy
(ie HAProxy) capable of monitoring MariaDB Galera Cluster nodes properly.

### Manually removing a node from the load balancer cluster list

When using in conjunction with load balancer you can remove specific node
from cluster list so it will stop receiving queries.
You can do that by touching /var/tmp/clustercheck.disabled on specific node,
an admin may force clustercheck to return 503, regardless as to the actual
state of the node.
This is useful when the node is being put into maintenance mode.

## License

Apache License, Version 2.0

## Author Information

Dobroslaw Zybort
